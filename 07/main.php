<?php

enum NormalCard: string
{
    case Ace = 'A';
    case King = 'K';
    case Queen = 'Q';
    case Jack = 'J';
    case Ten = 'T';
    case Nine = '9';
    case Eight = '8';
    case Seven = '7';
    case Six = '6';
    case Five = '5';
    case Four = '4';
    case Three = '3';
    case Two = '2';

    public function getOrderKey(): int
    {
        return match ($this) {
            self::Ace => 0,
            self::King => 1,
            self::Queen => 2,
            self::Jack => 3,
            self::Ten => 4,
            self::Nine => 5,
            self::Eight => 6,
            self::Seven => 7,
            self::Six => 8,
            self::Five => 9,
            self::Four => 10,
            self::Three => 11,
            self::Two => 12,
        };
    }
}

enum JokerCard: string
{
    case Ace = 'A';
    case King = 'K';
    case Queen = 'Q';
    case Ten = 'T';
    case Nine = '9';
    case Eight = '8';
    case Seven = '7';
    case Six = '6';
    case Five = '5';
    case Four = '4';
    case Three = '3';
    case Two = '2';
    case Joker = 'J';

    public function getOrderKey(): int
    {
        return match ($this) {
            self::Ace => 0,
            self::King => 1,
            self::Queen => 2,
            self::Ten => 3,
            self::Nine => 4,
            self::Eight => 5,
            self::Seven => 6,
            self::Six => 7,
            self::Five => 8,
            self::Four => 9,
            self::Three => 10,
            self::Two => 11,
            self::Joker => 12,
        };
    }
}

enum Type
{
    case FiveOfAKind;
    case FourOfAKind;
    case FullHouse;
    case ThreeOfAKind;
    case TwoPair;
    case OnePair;
    case HighCard;

    public function getOrderKey(): int
    {
        return match ($this) {
            self::FiveOfAKind => 0,
            self::FourOfAKind => 1,
            self::FullHouse => 2,
            self::ThreeOfAKind => 3,
            self::TwoPair => 4,
            self::OnePair => 5,
            self::HighCard => 6,
        };
    }

    public static function getTypeByFiveCardString(string $cards): self
    {
        $occurrences = count_chars($cards, 1);
        $amountOccurrences = array_count_values($occurrences);

        if (array_key_exists(5, $amountOccurrences)) {
            return self::FiveOfAKind;
        }

        if (array_key_exists(4, $amountOccurrences)) {
            return self::FourOfAKind;
        }

        if (array_key_exists(3, $amountOccurrences)) {
            if (array_key_exists(2, $amountOccurrences)) {
                return self::FullHouse;
            }
            return self::ThreeOfAKind;
        }

        if (array_key_exists(2, $amountOccurrences)) {
            return $amountOccurrences[2] > 1
                ? self::TwoPair
                : self::OnePair;
        }

        return self::HighCard;
    }

    public static function getTypeByFiveCardStringWithJoker(string $cards): self
    {
        $amountJoker = count_chars($cards, 1)[ord(JokerCard::Joker->value)] ?? 0;
        $typeWithoutJoker = self::getTypeByFiveCardString($cards);
        if ($amountJoker === 0) {
            return $typeWithoutJoker;
        }

        if ($amountJoker > 3) {
            return self::FiveOfAKind;
        }

        if ($amountJoker === 3) {
            return match ($typeWithoutJoker) {
                self::FullHouse => self::FiveOfAKind,
                self::ThreeOfAKind => self::FourOfAKind,
                self::FiveOfAKind, self::FourOfAKind, self::TwoPair, self::OnePair, self::HighCard => throw new LogicException(),
            };
        }

        if ($amountJoker === 2) {
            return match ($typeWithoutJoker) {
                self::FullHouse => self::FiveOfAKind,
                self::TwoPair => self::FourOfAKind,
                self::OnePair => self::ThreeOfAKind,
                self::FiveOfAKind, self::FourOfAKind, self::HighCard, self::ThreeOfAKind => throw new LogicException(),
            };
        }

        return match ($typeWithoutJoker) {
            self::FourOfAKind => self::FiveOfAKind,
            self::ThreeOfAKind => self::FourOfAKind,
            self::TwoPair => self::FullHouse,
            self::OnePair => self::ThreeOfAKind,
            self::HighCard => self::OnePair,
            self::FiveOfAKind, self::FullHouse => throw new LogicException(),
        };
    }
}

readonly class Game
{
    public function __construct(
        public Type   $type,
        public string $cardString,
        public int    $bid,
        public bool   $hasJoker = false,
    )
    {
    }
}

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$solution1 = playCardsAndCountWinnings($input, false);
$solution2 = playCardsAndCountWinnings($input, true);

echo sprintf("Solution 07-1: %s\n", $solution1);
echo sprintf("Solution 07-2: %s\n", $solution2);

function playCardsAndCountWinnings(array $input, bool $withJoker): int
{
    $cardResults = array_map(
        static function (string $inputLine) use ($withJoker): Game {
            [$cards, $bid] = explode(' ', $inputLine);

            return new Game(
                $withJoker
                    ? Type::getTypeByFiveCardStringWithJoker($cards)
                    : Type::getTypeByFiveCardString($cards),
                $cards,
                $bid,
                $withJoker && str_contains(JokerCard::Joker->value, $cards)
            );
        },
        $input
    );

    usort(
        $cardResults,
        static function (Game $game1, Game $game2) use ($withJoker): int {
            $type1Order = $game1->type->getOrderKey();
            $type2Order = $game2->type->getOrderKey();

            if ($type1Order !== $type2Order) {
                return $type1Order <=> $type2Order;
            }

            if ($game1->hasJoker) {
                return 1;
            }

            if ($game2->hasJoker) {
                return -1;
            }

            $cardEnumToUse = $withJoker ? 'JokerCard' : 'NormalCard';
            foreach (range(0, strlen($game1->cardString)) as $index) {
                $game1Card = $cardEnumToUse::from(str_split($game1->cardString)[$index])->getOrderKey();
                $game2Card = $cardEnumToUse::from(str_split($game2->cardString)[$index])->getOrderKey();
                if ($game1Card !== $game2Card) {
                    return $game1Card <=> $game2Card;
                }
            }

            throw new LogicException('que?');
        }
    );

    return array_sum(
        array_map(
            static fn(Game $game, int $rank): int => $game->bid * $rank,
            $cardResults, range(count($cardResults), 1)
        )
    );
}
