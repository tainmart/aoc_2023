<?php

$input = file($argv[1], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

$directions = array_shift($input);
$network = array_reduce(
    $input,
    static function (array $networkCarry, string $inputLine): array {
        preg_match('/(?<destination>\w{3}) = \((?<left>\w{3}), (?<right>\w{3})\)/', $inputLine, $parsedLine);
        $networkCarry[$parsedLine['destination']] = ['L' => $parsedLine['left'], 'R' => $parsedLine['right']];
        return $networkCarry;
    },
    []
);

$solution1 = calculateDistancesForStartPoints(['AAA'], $network, $directions)[0];

$allStartingPoints = array_filter(
    array_keys($network),
    static fn(string $node): bool => str_ends_with($node, 'A')
);
$solution2Distances = calculateDistancesForStartPoints($allStartingPoints, $network, $directions);
$solution2 = leastCommonMultipleArray($solution2Distances);

echo sprintf("Solution 08-1: %s\n", $solution1);
echo sprintf("Solution 08-2: %s\n", $solution2);


function calculateDistancesForStartPoints(array $startPoints, array $network, string $directions): array
{
    return array_map(
        static function (string $startPoint) use ($network, $directions): int {
            $countMoves = 0;
            $amountDirections = strlen($directions);

            $current = $startPoint;
            while (true) {
                if (str_ends_with($current, 'Z')) {
                    return $countMoves;
                }

                $direction = $directions[$countMoves % $amountDirections];
                $countMoves++;

                $current = $network[$current][$direction];
            }
        },
        $startPoints
    );
}

function greatestCommonDivisor(int $a, int $b): int
{
    return $b === 0 ? $a : greatestCommonDivisor($b, $a % $b);
}

function leastCommonMultiple(int $a, int $b): int
{
    return ($a * $b / greatestCommonDivisor($a, $b));
}

function leastCommonMultipleArray(array $numbers): int
{
   if (count($numbers) === 2) {
       return leastCommonMultiple(...$numbers);
   }

   return leastCommonMultiple(array_shift($numbers), leastCommonMultipleArray($numbers));
}
