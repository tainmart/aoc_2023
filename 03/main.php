<?php

readonly class SchematicOccurrence
{
    public function __construct(
        public string|int $value,
        public int        $row,
        public int        $column,
    )

    {
    }
}

function parseLineWithRegex(string $regex, string $line, int $lineNumber): array
{
    $occurrences = [];
    $countOccurrence = [];
    preg_match_all($regex, $line, $matches, PREG_OFFSET_CAPTURE);
    foreach ($matches[0] as [$match, $column]) {
        $countOccurrence[$match] = ($countOccurrence[$match] ?? 0) + 1;
        $occurrences[] = new SchematicOccurrence(
            $match,
            $lineNumber,
            $column
        );
    }
    return $occurrences;
}

function inRange(int $number, int $min, int $max)
{
    return $min <= $number && $number <= $max;
}

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$numbers = [];
$symbols = [];

foreach ($input as $index => $inputLine) {
    $numbers[] = parseLineWithRegex('/\d+/', $inputLine, $index);
    $symbols[] = parseLineWithRegex('/[^\.\d\s]/', $inputLine, $index);
}
$numbers = array_merge(...$numbers);
$symbols = array_merge(...$symbols);

$solution1 = array_reduce(
    $numbers,
    static function (int $carrySum, SchematicOccurrence $number) use ($symbols): int {
        $numberLength = strlen($number->value);
        foreach ($symbols as $symbol) {
            if (!inRange($symbol->row, $number->row - 1, $number->row + 1)) {
                continue;
            }

            if (!inRange($symbol->column, $number->column - 1, $number->column + $numberLength)) {
                continue;
            }

            return $carrySum + $number->value;
        }

        return $carrySum;
    },
    0
);

$solution2 = array_reduce(
    $symbols,
    static function (int $carrySum, SchematicOccurrence $symbol) use ($numbers): int {
        $matchingNumbers = [];
        foreach ($numbers as $number) {
            $numberLength = strlen($number->value);
            if (!inRange($symbol->row, $number->row - 1, $number->row + 1)) {
                continue;
            }
            if (!inRange($symbol->column, $number->column - 1, $number->column + $numberLength)) {
                continue;
            }
            $matchingNumbers[] = $number->value;
        }

        if (count($matchingNumbers) === 2) {
            return array_product($matchingNumbers) + $carrySum;
        }

        return $carrySum;
    },
    0
);

echo sprintf("Solution 03-1: %s\n", $solution1);
echo sprintf("Solution 03-2: %s\n", $solution2);
