<?php

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$solution1 = shoelace(
    ...array_reduce(
        $input,
        static function (array $carry, string $inputLine): array {
            [$direction, $length] = explode(' ', $inputLine);
            $carry['directions'][] = $direction;
            $carry['lengths'][] = $length;

            return $carry;
        },
        ['directions' => [], 'lengths' => []]
    )
);
$solution2 = shoelace(
    ...array_reduce(
        $input,
        static function (array $carry, string $inputLine): array {
            [$direction, $length, $color] = explode(' ', $inputLine);
            $color = trim($color, '(#)');
            $carry['directions'][] = match (substr($color, -1)) {
                '0' => 'R',
                '1' => 'D',
                '2' => 'L',
                '3' => 'U',
            };
            $carry['lengths'][] = hexdec(substr($color, 0, 5));

            return $carry;
        },
        ['directions' => [], 'lengths' => []]
    )
);

echo sprintf("Solution 18-1: %s\n", $solution1);
echo sprintf("Solution 18-2: %s\n", $solution2);

// math 🤷
// https://artofproblemsolving.com/wiki/index.php/Shoelace_Theorem
function shoelace(array $directions, array $lengths): int
{
    $x1 = 0;
    $x2 = 0;
    $y1 = 0;
    $y2 = 0;
    $totalArea = 0;
    $border = 0;

    for ($index = 0, $indexMax = count($directions); $index < $indexMax; $index++) {
        $direction = $directions[$index];
        $length = $lengths[$index];

        $border += $length;

        match ($direction) {
            'U' => $y2 -= $length,
            'D' => $y2 += $length,
            'R' => $x2 += $length,
            'L' => $x2 -= $length,
        };

        $totalArea += ($x2 + $x1) * ($y2 - $y1);
        [$x1, $y1] = [$x2, $y2];
    }

    return ($totalArea + $border) / 2 + 1;
}
