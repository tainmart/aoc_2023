<?php

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$conditionRecords = array_map(
    static function (string $inputLine): array {
        [$conditions, $damagedListString] = explode(' ', $inputLine);
        $damagedList = array_map('intval', explode(',', $damagedListString));

        return [$conditions, $damagedList];
    },
    $input
);

$solution1 = array_sum(
    array_map(
        static fn(array $conditionRecord): int => getPossiblePermutationCount(...$conditionRecord),
        $conditionRecords
    )
);

$conditionRecord = $conditionRecords[0];
$solution2 = array_sum(
    array_map(
        static function (array $conditionRecord): int {
            [$conditions, $damagedList] = $conditionRecord;
            return getPossiblePermutationCount(
                implode('?', array_fill(0, 5, $conditions)),
                array_merge(...array_fill(0, 5, $damagedList))
            );
        },
        $conditionRecords
    )
);

echo sprintf("Solution 12-1: %s\n", $solution1);
echo sprintf("Solution 12-2: %s\n", $solution2);

function getPossiblePermutationCount(
    string $conditions,
    array  $damagedList,
    int    $currentIndex = 0,
    int    $currentDamagedSequenceLength = 0,
    int    $countFinishedDamagedSequences = 0,
    array  &$cache = []
): int
{
    $cacheKey = sprintf('%s-%s-%s', $currentIndex, $currentDamagedSequenceLength, $countFinishedDamagedSequences);
    if (array_key_exists($cacheKey, $cache)) {
        return $cache[$cacheKey];
    }

    $countDamagedList = count($damagedList);
    if ($currentIndex === strlen($conditions)) {
        if ($countFinishedDamagedSequences === $countDamagedList - 1 && $currentDamagedSequenceLength === $damagedList[$countFinishedDamagedSequences]) {
            $countFinishedDamagedSequences++;
            $currentDamagedSequenceLength = 0;
        }
        return $countFinishedDamagedSequences === $countDamagedList && $currentDamagedSequenceLength === 0
            ? 1
            : 0;
    }

    $amountPermutations = 0;

    $currentCondition = $conditions[$currentIndex];
    if ($currentCondition === '.' || $currentCondition === '?') {
        if ($currentDamagedSequenceLength === 0) {
            $amountPermutations += getPossiblePermutationCount(
                $conditions,
                $damagedList,
                $currentIndex + 1,
                0,
                $countFinishedDamagedSequences,
                $cache,
            );
        } elseif ($countFinishedDamagedSequences < $countDamagedList && $damagedList[$countFinishedDamagedSequences] === $currentDamagedSequenceLength) {
            $amountPermutations += getPossiblePermutationCount(
                $conditions,
                $damagedList,
                $currentIndex + 1,
                0,
                $countFinishedDamagedSequences + 1,
                $cache,
            );
        }
    }

    if ($currentCondition === '#' || $currentCondition === '?') {
        $amountPermutations += getPossiblePermutationCount(
            $conditions,
            $damagedList,
            $currentIndex + 1,
            $currentDamagedSequenceLength + 1,
            $countFinishedDamagedSequences,
            $cache
        );
    }

    $cache[$cacheKey] = $amountPermutations;
    return $amountPermutations;
}
