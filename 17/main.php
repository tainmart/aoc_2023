<?php

enum Direction: string
{
    case Up = 'up';
    case Right = 'right';
    case Down = 'down';
    case Left = 'left';

    function isOppositeDirection(Direction $newDirection): bool
    {
        return match ($this) {
            self::Up => $newDirection === self::Down,
            self::Right => $newDirection === self::Left,
            self::Down => $newDirection === self::Up,
            self::Left => $newDirection === self::Right,
        };
    }
}

$input = array_map(
    'str_split',
    file($argv[1], FILE_IGNORE_NEW_LINES)
);

$solution1 = depthFirstSearch($input, 1, 3);
$solution2 = depthFirstSearch($input, 4, 10);

echo sprintf("Solution 17-1: %s\n", $solution1);
echo sprintf("Solution 17-2: %s\n", $solution2);

function depthFirstSearch(
    array $field,
    int   $minStepDistance,
    int   $maxStepDistance,
): int
{
    $countRows = count($field);
    $countColumns = count($field[0]);
    $destinationRow = $countRows - 1;
    $destinationColumn = $countColumns - 1;

    // heatValue, row, column, direction, stepDistance
    // used array + sort, but this is so much faster 🤯
    $queue = new SplMinHeap();
    $queue->insert([0, 0, 0, Direction::Right, 0]);

    $visited = [];
    $minimalHeatValue = INF;

    while (!$queue->isEmpty()) {
        [$pathHeatValue, $row, $column, $direction, $stepDistance] = $queue->extract();
        $keyHash = sprintf('%s-%s-%s-%s', $row, $column, $direction->value, $stepDistance);
        // isset is way faster than in_array or array_key_exists 👀
        if (isset($visited[$keyHash])) {
            continue;
        }
        $visited[$keyHash] = '';

        if ($row === $destinationRow && $column === $destinationColumn && $stepDistance >= $minStepDistance) {
            $minimalHeatValue = min($pathHeatValue, $minimalHeatValue);
            continue;
        }

        foreach (Direction::cases() as $newDirection) {
            if ($direction->isOppositeDirection($newDirection)) {
                continue;
            }

            if ($direction !== $newDirection && $stepDistance < $minStepDistance) {
                continue;
            }

            $newStepDistance = $direction === $newDirection
                ? $stepDistance + 1
                : 1;

            if ($newStepDistance > $maxStepDistance) {
                continue;
            }
            [$newRow, $newColumn] = match ($newDirection) {
                Direction::Up => [$row - 1, $column],
                Direction::Down => [$row + 1, $column],
                Direction::Right => [$row, $column + 1],
                Direction::Left => [$row, $column - 1]
            };

            if ($newRow < 0
                || $newRow > $destinationRow
                || $newColumn < 0
                || $newColumn > $destinationColumn
            ) {
                continue;
            }

            $queue->insert([
                    $pathHeatValue + $field[$newRow][$newColumn],
                    $newRow,
                    $newColumn,
                    $newDirection,
                    $newStepDistance
                ]
            );
        }
    }

    return $minimalHeatValue;
}
