<?php

const MAXIMUM_PER_COLOR = [
    'red' => 12,
    'green' => 13,
    'blue' => 14
];

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$solution1 = array_reduce(
    $input,
    static function (int $sumValidGames, string $inputLine): int {
        [$game, $cubes] = explode(':', $inputLine);
        $gameNumber = (int)filter_var($game, FILTER_SANITIZE_NUMBER_INT);
        $moveSets = explode(';', $cubes);

        foreach ($moveSets as $moveSet) {
            $moves = explode(',', $moveSet);
            foreach ($moves as $move) {
                [$countCubes, $color] = explode(' ', trim($move));
                if ((int)$countCubes > MAXIMUM_PER_COLOR[$color]) {
                    return $sumValidGames;
                }
            }
        }

        return $sumValidGames + $gameNumber;
    },
    0,
);

$solution2 = array_sum(
    array_map(
        static function (string $inputLine): int {
            [$game, $cubes] = explode(':', $inputLine);
            $moveSets = explode(';', $cubes);

            $minimumAmountPerColor = [];
            foreach ($moveSets as $moveSet) {
                $moves = explode(',', $moveSet);
                foreach ($moves as $move) {
                    [$countCubes, $color] = explode(' ', trim($move));
                    $minimumAmountPerColor[$color] = max($countCubes, $minimumAmountPerColor[$color] ?? 0);
                }
            }

            return array_product($minimumAmountPerColor);
        },
        $input,
    )
);

echo sprintf("Solution 02-1: %s\n", $solution1);
echo sprintf("Solution 02-2: %s\n", $solution2);

