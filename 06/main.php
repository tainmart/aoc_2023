<?php

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

preg_match_all('/\d+/', $input[0], $times);
preg_match_all('/\d+/', $input[1], $records);

$combinedRecordsAndTimes = [];
foreach ($times[0] as $index => $time) {
    $combinedRecordsAndTimes[] = [$time, $records[0][$index]];
}
$solution1 = array_product(
    array_map(
        static fn(array $timeAndRecord): int => calculateAmountOfRaceWinner(...$timeAndRecord),
        $combinedRecordsAndTimes
    )
);

$singleTimeRecord = [
    (int)filter_var($input[0], FILTER_SANITIZE_NUMBER_INT),
    (int)filter_var($input[1], FILTER_SANITIZE_NUMBER_INT),
];

$solution2 = calculateAmountOfRaceWinner(...$singleTimeRecord);

echo sprintf("Solution 06-1: %s\n", $solution1);
echo sprintf("Solution 06-2: %s\n", $solution2);

function calculateAmountOfRaceWinner(int $time, int $record): int
{
    // t total_time
    // r record
    // x current_time
    // -> solve for x > 0, then we have our start and end range

    // t = 7, r = 9
    // x * (t - x) > r
    // tx - x² > r
    // -x² + tx > r
    // -x² + tx - r > 0
    // x² - tx + r = 0

    // pq time
    // [from,to] = t/2 +- sqrt((t/2)² - r)

    $to = $time / 2 + sqrt((($time / 2) ** 2) - $record);
    $from = (int)($time / 2 - sqrt((($time / 2) ** 2) - $record));

    // since $to is the upper limit, we need to round down
    // if $to is a natural number, we need to subtract 1
    $to = floor($to) === $to
        ? (int)$to - 1
        : (int)$to;

    return $to - $from;
}
