<?php

enum Direction
{
    case North;
    case East;
    case South;
    case West;
}

enum Pipe: string
{
    case Start = 'S';
    case Ground = '.';
    case Vertical = '|';
    case Horizontal = '-';
    case NorthEast = 'L';
    case NorthWest = 'J';
    case SouthEast = 'F';
    case SouthWest = '7';
}

$input = file($argv[1], FILE_IGNORE_NEW_LINES);
$allPipes = array_map('str_split', $input);

['column' => $currentColumn, 'row' => $currentRow] = getStartingPosition($allPipes);

// find one of the two pipes connecting S
if (in_array($allPipes[$currentRow - 1][$currentColumn] ?? '.', ['|', '7', 'F'], true)) {
    $direction = Direction::North;
} elseif (in_array($allPipes[$currentRow][$currentColumn - 1] ?? '.', ['-', 'L', 'F'], true)) {
    $direction = Direction::East;
} elseif (in_array($allPipes[$currentRow + 1][$currentColumn] ?? '.', ['|', 'L', 'J'], true)) {
    $direction = Direction::South;
} else {
    $direction = Direction::West;
}

$animalPipes = [];
$animalPipes[$currentRow][$currentColumn] = 'S';

match ($direction) {
    Direction::North => $currentRow--,
    Direction::East => $currentColumn--,
    Direction::South => $currentRow++,
    Direction::West => $currentColumn++,
};

while ($allPipes[$currentRow][$currentColumn] !== 'S') {
    // update new direction
    $currentPipe = $allPipes[$currentRow][$currentColumn];
    $direction = getNewDirectionByPreviousDirectionAndCurrentPipe($direction, $currentPipe);

    $animalPipes[$currentRow][$currentColumn] = $currentPipe;

    match ($direction) {
        Direction::North => $currentRow--,
        Direction::East => $currentColumn--,
        Direction::South => $currentRow++,
        Direction::West => $currentColumn++,
    };
}

$insideTiles = [];
foreach ($allPipes as $rowIndex => $row) {
    foreach ($row as $columnIndex => $field) {
        if (isset($animalPipes[$rowIndex][$columnIndex])) {
            continue;
        }
        $northFacingPipes = 0;
        for ($index = $columnIndex - 1; $index >= 0; $index--) {
            if (!isset($animalPipes[$rowIndex][$index])) {
                continue;
            }
            if (in_array(Pipe::from($row[$index]), [Pipe::Vertical, Pipe::NorthEast, Pipe::NorthWest], true)) {
                $northFacingPipes++;
            }
        }
        if ($northFacingPipes % 2 === 0) {
            continue;
        }

        $insideTiles[$rowIndex][$columnIndex] = $field;
    }
}

$solution1 = array_sum(array_map('count', $animalPipes)) / 2;
$solution2 = array_sum(array_map('count', $insideTiles));

echo sprintf("Solution 10-1: %s\n", $solution1);
echo sprintf("Solution 10-2: %s\n", $solution2);

function getStartingPosition(array $pipes): array
{
    foreach ($pipes as $rowIndex => $row) {
        foreach ($row as $columnIndex => $pipeSymbol) {
            if ($pipeSymbol !== 'S') {
                continue;
            }

            return ['row' => $rowIndex, 'column' => $columnIndex];
        }
    }
    throw new UnexpectedValueException();
}

function getNewDirectionByPreviousDirectionAndCurrentPipe(Direction $previousDirection, string $currentPipe): Direction
{
    if ($previousDirection === Direction::North) {
        return match (Pipe::from($currentPipe)) {
            Pipe::Vertical => Direction::North,
            Pipe::SouthEast => Direction::West,
            Pipe::SouthWest => Direction::East,
            default => throw new LogicException()
        };
    }

    if ($previousDirection === Direction::East) {
        return match (Pipe::from($currentPipe)) {
            Pipe::Horizontal => Direction::East,
            Pipe::NorthEast => Direction::North,
            Pipe::SouthEast => Direction::South,
            default => throw new LogicException()
        };
    }
    if ($previousDirection === Direction::South) {
        return match (Pipe::from($currentPipe)) {
            Pipe::Vertical => Direction::South,
            Pipe::NorthEast => Direction::West,
            Pipe::NorthWest => Direction::East,
            default => throw new LogicException()
        };
    }

    return match (Pipe::from($currentPipe)) {
        Pipe::Horizontal => Direction::West,
        Pipe::NorthWest => Direction::North,
        Pipe::SouthWest => Direction::South,
        default => throw new LogicException()
    };
}
