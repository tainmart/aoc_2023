<?php

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$parsedLines = array_map(
    static function (string $inputLine) {
        [$_, $numbers] = explode(':', $inputLine);
        [$winningString, $yourString] = explode('|', $numbers);
        return [
            preg_split('/\s+/', trim($winningString)),
            preg_split('/\s+/', trim($yourString)),
        ];
    },
    $input
);

$solution1 = array_sum(
    array_map(
        static function (array $numbers) {
            $matches = count(array_intersect(...$numbers));
            return $matches > 0
                ? pow(2, $matches - 1)
                : 0;
        },
        $parsedLines
    )
);

$countGames = count($parsedLines);
$countCardsMap = array_fill(0, $countGames, 1);
foreach ($parsedLines as $cardNumber => $numbers) {
    $matches = count(array_intersect(...$numbers));
    for ($i = 1; $i <= $matches; $i++) {
        if ($cardNumber + $i > $countGames) {
            break;
        }
        $countCardsMap[$cardNumber + $i] += $countCardsMap[$cardNumber];
    }
}
$solution2 = array_sum($countCardsMap);

echo sprintf("Solution 04-1: %s\n", $solution1);
echo sprintf("Solution 04-2: %s\n", $solution2);
