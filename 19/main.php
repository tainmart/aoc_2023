<?php

[$workflows, $ratings] = array_map(
    static fn(string $inputPart) => explode("\n", $inputPart),
    explode("\n\n", trim(file_get_contents($argv[1])))
);

$mappedWorkflows = mapWorkflows($workflows);
$mappedRatings = mapRatings($ratings);

$solution1 = array_sum(
    array_map(
        static function (array $rating) use ($mappedWorkflows): int {
            $nextWorkflow = 'in';
            while ($nextWorkflow !== 'A' && $nextWorkflow !== 'R') {
                foreach ($mappedWorkflows[$nextWorkflow] as $workflow) {
                    if ($workflow['condition']($rating)) {
                        $nextWorkflow = $workflow['destination'];
                        continue 2;
                    }
                }
            }

            return $nextWorkflow === 'R'
                ? 0
                : array_sum($rating);
        },
        $mappedRatings
    )
);

$solution2 = getRangeResultForWorkflows($workflows);

echo sprintf("Solution 19-1: %s\n", $solution1);
echo sprintf("Solution 19-2: %s\n", $solution2);

function mapWorkflows(array $workflows): array
{
    $mappedWorkflows = [];
    foreach ($workflows as $workflow) {
        [$name, $rules] = explode('{', trim($workflow, '}'));
        foreach (explode(',', $rules) as $rule) {
            if (!str_contains($rule, ':')) {
                $mappedWorkflows[$name][] = [
                    'condition' => static fn(array $rating) => true,
                    'destination' => $rule
                ];
                continue;
            }
            [$condition, $destination] = explode(':', $rule);
            if (str_contains($condition, '<')) {
                [$ratingPart, $value] = explode('<', $condition);
                $mappedWorkflows[$name][] = [
                    'condition' => static fn(array $rating) => $rating[$ratingPart] < $value,
                    'destination' => $destination
                ];
            } else {
                [$ratingPart, $value] = explode('>', $condition);
                $mappedWorkflows[$name][] = [
                    'condition' => static fn(array $rating) => $rating[$ratingPart] > $value,
                    'destination' => $destination
                ];

            }
        }
    }

    while (true) {
        foreach ($mappedWorkflows as $name => $workflow) {
            $uniqueDestinations = array_unique(
                array_column($workflow, 'destination')
            );
            if (count($uniqueDestinations) === 1) {
                if ($name === 'A' || $name === 'R') {
                    continue;
                }
                unset($mappedWorkflows[$name]);

                $replaceDestination = $uniqueDestinations[0];
                array_walk_recursive(
                    $mappedWorkflows,
                    static function (mixed &$value, mixed $key) use ($name, $replaceDestination) {
                        if ($key !== 'destination' || $value !== $name) {
                            return;
                        }
                        $value = $replaceDestination;
                    }
                );

                continue 2;
            }
        }

        break;
    }
    // remove everything ending in R

    return $mappedWorkflows;
}

function mapRatings(array $ratings): array
{
    return array_map(
        static function (string $ratingLine): array {
            [$x, $m, $a, $s] = array_map(
                static fn(string $rating) => (int)explode('=', $rating)[1],
                explode(',', trim($ratingLine, '{}'))
            );
            return [
                'x' => $x,
                'm' => $m,
                'a' => $a,
                's' => $s,
            ];
        },
        $ratings
    );
}

function getRangeResultForWorkflows(array $workflows): int
{
    $mappedWorkflows = [];
    foreach ($workflows as $workflow) {
        [$name, $rules] = explode('{', trim($workflow, '}'));
        foreach (explode(',', $rules) as $nextWorkflow) {
            if (!str_contains($nextWorkflow, ':')) {
                $mappedWorkflows[$name][] = [
                    'condition' => [],
                    'nextWorkflow' => $nextWorkflow
                ];
                continue;
            }
            [$condition, $nextWorkflow] = explode(':', $nextWorkflow);
            $operator = str_contains($condition, '<') ? '<' : '>';
            [$ratingPart, $workflowValue] = explode($operator, $condition);
            $mappedWorkflows[$name][] = [
                'condition' => ['ratingPart' => $ratingPart, 'value' => (int)$workflowValue, 'operator' => $operator],
                'nextWorkflow' => $nextWorkflow
            ];
        }
    }

    $ratingReducers = [
        [
            'range' => [
                'x' => ['min' => 1, 'max' => 4000],
                'm' => ['min' => 1, 'max' => 4000],
                'a' => ['min' => 1, 'max' => 4000],
                's' => ['min' => 1, 'max' => 4000],
            ],
            'workflow' => 'in',
            'ruleNumber' => 0
        ]
    ];
    $result = 0;

    while (!empty($ratingReducers)) {
        $ratingReducer = array_pop($ratingReducers);

        $workflowName = $ratingReducer['workflow'];
        if ('R' === $workflowName) {
            continue;
        }

        if ('A' === $workflowName) {
            $result += array_product(
                array_map(
                    static fn(array $range) => $range['max'] - $range['min'] + 1,
                    $ratingReducer
                )
            );
            continue;
        }

        // ^^^^ so far so good

        $workflow = $mappedWorkflows[$workflowName][$ratingReducer['ruleNumber']];

        if (empty($workflow['condition'])) {
            $ratingReducers[] = [
                'range' => $ratingReducer['range'],
                'workflow' => $ratingReducer['workflow'],
                'ruleNumber' => 0
            ];
            continue;
        }

        $matched = [
            'range' => $ratingReducer['range'],
            'workflow' => $ratingReducer['workflow'],
            'ruleNumber' => 0
        ];
        $notMatched = [
            'range' => $ratingReducer['range'],
            'workflow' => $ratingReducer['workflow'],
            'ruleNumber' => $ratingReducer['ruleNumber'] + 1
        ];

        [
            'operator' => $operator,
            'ratingPart' => $ratingPart ,
            'value' => $workflowValue,
        ] = $workflow['condition'];
        if ('<' === $operator) {
            $matched['range'][$ratingPart]['max'] = min(
                $ratingReducer['range'][$ratingPart]['max'],
                $workflowValue - 1
            );
            $notMatched['range'][$ratingPart]['min'] = max(
                $ratingReducer['range'][$ratingPart]['min'],
                $workflowValue
            );
        } elseif ('>' === $operator) {
            $matched['range'][$ratingPart]['min'] = max(
                $ratingReducer['range'][$ratingPart]['min'],
                $workflowValue + 1
            );
            $notMatched[$workflow['ratingPart']]['max'] = min(
                $ratingReducer['range'][$ratingPart]['max'],
                $workflowValue
            );
        }

        if ($matched['range'][$ratingPart]['min'] <= $matched['range'][$ratingPart]['max']) {
            $ratingReducers[] = $matched;
        }

        if ($notMatched['range'][$ratingPart]['min'] <= $notMatched['range'][$ratingPart]['max']) {
            $ratingReducers[] = $notMatched;
        }
    }

    return $result;
}

