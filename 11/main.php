<?php

$input = array_map('str_split', file($argv[1], FILE_IGNORE_NEW_LINES));

$galaxyPositions = determineGalaxyPositions($input);
$galaxyPositionsPart1 = updateGalaxyPositionsForEmptySpaces($galaxyPositions, $input, 1);
$galaxyPositionsPart2 = updateGalaxyPositionsForEmptySpaces($galaxyPositions, $input, 1_000_000 - 1);

$solution1 = calculateSumShortestDistances($galaxyPositionsPart1);
$solution2 = calculateSumShortestDistances($galaxyPositionsPart2);

echo sprintf("Solution 11-1: %s\n", $solution1);
echo sprintf("Solution 11-2: %s\n", $solution2);

function determineGalaxyPositions(array $space): array
{
    $row = 0;
    return array_reduce(
        $space,
        static function (array $positionsCarry, array $arrayRow) use (&$row): array {
            foreach ($arrayRow as $column => $symbol) {
                if ($symbol === '.') {
                    continue;
                }
                $positionsCarry[] = ['row' => $row, 'column' => $column];
            }
            $row++;
            return $positionsCarry;
        },
        []
    );
}

function updateGalaxyPositionsForEmptySpaces(array $galaxyPositions, array $space, int $spaceExpansion): array
{
    foreach (array_reverse($space, true) as $rowIndex => $row) {
        if (in_array('#', $row, true)) {
            continue;
        }
        foreach ($galaxyPositions as $galaxyIndex => $galaxyPosition) {
            if ($galaxyPosition['row'] > $rowIndex) {
                $galaxyPositions[$galaxyIndex] = [
                    'row' => $galaxyPosition['row'] + $spaceExpansion,
                    'column' => $galaxyPosition['column']
                ];
            }
        }
    }

    $transposedSpace = array_map(null, ...$space);
    foreach (array_reverse($transposedSpace, true) as $columnIndex => $column) {
        if (in_array('#', $column, true)) {
            continue;
        }
        foreach ($galaxyPositions as $galaxyIndex => $galaxyPosition) {
            if ($galaxyPosition['column'] > $columnIndex) {
                $galaxyPositions[$galaxyIndex] = [
                    'row' => $galaxyPosition['row'],
                    'column' => $galaxyPosition['column'] + $spaceExpansion
                ];
            }
        }
    }

    return $galaxyPositions;
}

function calculateSumShortestDistances(array $galaxyPositions): int
{
    $sumShortestDistances = 0;
    $amountGalaxies = count($galaxyPositions);
    for ($i = 0; $i < $amountGalaxies; $i++) {
        for ($j = $i + 1; $j < $amountGalaxies; $j++) {
            $sumShortestDistances +=
                abs($galaxyPositions[$i]['row'] - $galaxyPositions[$j]['row'])
                + abs($galaxyPositions[$i]['column'] - $galaxyPositions[$j]['column']);
        }
    }
    return $sumShortestDistances;
}
