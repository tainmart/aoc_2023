<?php

const SPIN_CYCLES = 1_000_000_000;

enum Direction
{
    case North;
    case East;
    case South;
    case West;
}

$input = array_map('str_split', file($argv[1], FILE_IGNORE_NEW_LINES));


$solution1Field = $input;
tiltField($solution1Field, Direction::North);
$solution1 = getNorthSupportLoad($solution1Field);
$solution2 = getNorthSupportLoad(spinCycleManyTimes($input));

echo sprintf("Solution 14-1: %s\n", $solution1);
echo sprintf("Solution 14-2: %s\n", $solution2);

function tiltField(array &$field, Direction $direction): void
{
    $amountRows = count($field);
    $amountColumns = count($field[0]);
    $rocksMoved = true;
    while ($rocksMoved) {
        $rocksMoved = false;
        for ($row = 0; $row < $amountRows; $row++) {
            for ($column = 0; $column < $amountColumns; $column++) {
                if ($field[$row][$column] !== 'O') {
                    continue;
                }

                [$tiltRow, $tiltColumn] = match ($direction) {
                    Direction::North => [$row - 1, $column],
                    Direction::West => [$row, $column - 1],
                    Direction::South => [$row + 1, $column],
                    Direction::East => [$row, $column + 1],
                };
                if (($field[$tiltRow][$tiltColumn] ?? '?') === '.') {
                    $rocksMoved = true;
                    $field[$tiltRow][$tiltColumn] = 'O';
                    $field[$row][$column] = '.';
                }
            }
        }
    }
}

function getNorthSupportLoad(array $field): int
{
    $amountRows = count($field);
    return array_sum(
        array_map(
            static fn(array $line, int $index): int => (array_count_values($line)['O'] ?? 0) * ($amountRows - $index),
            $field,
            array_keys($field),
        )
    );
}

function spinCycleManyTimes(array $field): array
{
    $previousFields = [];
    while (true) {
        tiltField($field, Direction::North);
        tiltField($field, Direction::West);
        tiltField($field, Direction::South);
        tiltField($field, Direction::East);

        $previousMatch = array_search($field, $previousFields, true);
        if ($previousMatch !== false) {
            $currentIndex = count($previousFields);
            $sequenceLength = $currentIndex - $previousMatch;

            $billionthOffset = SPIN_CYCLES % $sequenceLength;
            $billionthField = $sequenceLength + $billionthOffset - 1;

            return $previousFields[$billionthField];

        }

        $previousFields[] = $field;
    }
}
