<?php

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$solution1 = array_sum(
    array_map(
        static function (string $inputLine): int {
            $numberString = preg_replace('/\D+/', '', $inputLine);
            return (int)sprintf(
                '%s%s',
                substr($numberString, 0, 1),
                substr($numberString, -1)
            );
        },
        $input
    )
);

$numbersToCheck = [
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,

    'one' => 1,
    'two' => 2,
    'three' => 3,
    'four' => 4,
    'five' => 5,
    'six' => 6,
    'seven' => 7,
    'eight' => 8,
    'nine' => 9,
];

$solution2 = array_sum(
    array_reduce(
        $input,
        static function (array $parsedNumbersPerLine, string $inputLine) use ($numbersToCheck): array {
            $currentNumbersArray = [];
            foreach ($numbersToCheck as $numberToCheck => $numberValue) {
                $firstIndex = strpos($inputLine, $numberToCheck);
                if (is_int($firstIndex)) {
                    $currentNumbersArray[$firstIndex] = $numberValue;
                }

                $lastIndex = strrpos($inputLine, $numberToCheck);
                if (is_int($lastIndex)) {
                    $currentNumbersArray[$lastIndex] = $numberValue;
                }
            }
            ksort($currentNumbersArray);
            $numberString = implode($currentNumbersArray);

            $parsedNumbersPerLine[] = (int)sprintf(
                '%s%s',
                substr($numberString, 0, 1),
                substr($numberString, -1)
            );
            return $parsedNumbersPerLine;
        },
        []
    )
);

echo sprintf("Solution 01-1: %s\n", $solution1);
echo sprintf("Solution 01-2: %s\n", $solution2);

