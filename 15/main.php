<?php

$input = trim(file_get_contents($argv[1]));

$solution1 = array_sum(
    array_map(
        'applyHashToString',
        explode(',', $input)
    )
);

$lensBoxes = sortLenses($input);
$solution2 = array_sum(
    array_map(
        static fn(array $boxes, int $boxNumber): int => array_sum(
            array_map(
                static fn(int $focalLength, int $slot) => ($boxNumber + 1) * ($slot + 1) * $focalLength,
                $boxes, array_keys(array_values($boxes))
            )
        ),
        $lensBoxes, array_keys($lensBoxes)
    )
);

echo sprintf("Solution 15-1: %s\n", $solution1);
echo sprintf("Solution 15-2: %s\n", $solution2);

function applyHashToString(string $string): int
{
    return array_reduce(
        str_split($string),
        static fn(int $carry, string $char): int => (($carry + ord($char)) * 17) % 256,
        0
    );
}

function sortLenses(string $input): array
{
    return array_reduce(
        explode(',', $input),
        static function (array $lensBoxes, string $step): array {
            if (str_contains($step, '=')) {
                [$boxLabel, $focalLength] = explode('=', $step);
                $boxNumber = applyHashToString($boxLabel);
                $lensBoxes[$boxNumber][$boxLabel] = $focalLength;
                return $lensBoxes;
            }

            $boxLabel = rtrim($step, '-');
            $boxNumber = applyHashToString($boxLabel);
            unset($lensBoxes[$boxNumber][$boxLabel]);

            return $lensBoxes;
        },
        array_fill(0, 255, [])
    );

    return $lensBoxes;
}
