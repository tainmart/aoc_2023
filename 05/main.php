<?php

readonly class RangeMapping
{
    public function __construct(
        public int $destinationStart,
        public int $sourceStart,
        public int $length,
    )
    {
    }
}

readonly class Range
{
    public function __construct(
        public int $start,
        public int $end
    )
    {
    }
}

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

preg_match_all('/\d+/', array_shift($input), $seedMatches);
$seeds = array_map('intval', $seedMatches[0]);
$mappings = parseMappingRanges($input);

$solution1 = determineMinimumLocationForSeedRanges(
    array_map(
        static fn(int $seed): Range => new Range($seed, $seed),
        $seeds
    ),
    $mappings
);

$solution2 = determineMinimumLocationForSeedRanges(
    array_map(
        static fn(array $seedRange): Range => new Range($seedRange[0], array_sum($seedRange) - 1),
        array_chunk($seeds, 2)
    ),
    $mappings
);

echo sprintf("Solution 05-1: %s\n", $solution1);
echo sprintf("Solution 05-2: %s\n", $solution2);

function parseMappingRanges(array $input): array
{
    $parseNewMap = true;
    $currentMapping = null;
    $mappings = [];
    foreach ($input as $line) {
        if (strlen($line) === 0) {
            $parseNewMap = true;
            continue;
        }

        if ($parseNewMap) {
            preg_match('/(?<from>\w+)-to-(?<to>\w+) /', $line, $mapNames);
            $currentMapping = sprintf('%s-%s', $mapNames['from'], $mapNames['to']);
            $parseNewMap = false;
            continue;
        }

        $mappings[$currentMapping][] = new RangeMapping(...array_map('intval', explode(' ', $line)));
    }
    return $mappings;
}

function determineMinimumLocationForSeedRanges(array $inputSeedRanges, array $maps): int
{
    $changedRanges = $inputSeedRanges;

    // iterate over all mappings
    foreach ($maps as $map) {
        $rangesAfterCompleteMap = [];

        // iterate over all ranges, which are changing after each input
        foreach ($changedRanges as $range) {
            $rangesForCurrentMap = [];
            $changedInitialInput = [];

            $rangeStart = $range->start;
            $rangeEnd = $range->end;

            // iterate over each map for one step
            foreach ($map as $mapping) {
                $sourceStart = $mapping->sourceStart;
                $sourceEnd = $sourceStart + $mapping->length;

                if ($rangeStart <= $sourceEnd && $sourceStart <= $rangeEnd) {
                    $mappedMin = max($rangeStart, $sourceStart);
                    $mappedMax = min($rangeEnd, $sourceEnd);

                    $offset = $mapping->destinationStart - $sourceStart;

                    // seeds are entirely in source map range, no need to check other maps
                    if ($sourceStart <= $rangeStart && $rangeEnd <= $sourceEnd) {
                        $rangesForCurrentMap = [new Range($offset + $mappedMin, $offset + $mappedMax)];
                        $changedInitialInput = [];
                        break;
                    }

                    $rangesForCurrentMap[] = new Range($offset + $mappedMin, $offset + $mappedMax);
                    $changedInitialInput[] = new Range($mappedMin, $mappedMax);
                }
            }
            // no changes or all changes will have empty changedInitialInput
            if (empty($changedInitialInput)) {
                // if there are no current ranges, then no input was changed and we can keep current mapping
                if (empty($rangesForCurrentMap)) {
                    $rangesForCurrentMap = [$range];
                }
                $rangesAfterCompleteMap = [...$rangesAfterCompleteMap, ...$rangesForCurrentMap];
                continue;
            }

            // sort changedInitialInput
            usort(
                $changedInitialInput,
                static fn(Range $range1, Range $range2) => $range1->start <=> $range2->start
            );


            // check for any gaps between changedInitialInput
            $previousChangedEnd = $rangeStart - 1;
            foreach ($changedInitialInput as $changedRange) {
                if ($changedRange->start - $previousChangedEnd > 1) {
                    $rangesForCurrentMap[] = new Range($previousChangedEnd + 1, $changedRange->start - 1);
                }
                $previousChangedEnd = $changedRange->end;
            }
            // and check for gap after last changedInitialInput
            if ($rangeEnd - $previousChangedEnd > 1) {
                $rangesForCurrentMap[] = new Range($previousChangedEnd + 1, $rangeEnd);
            }
            $rangesAfterCompleteMap = [...$rangesAfterCompleteMap, ...$rangesForCurrentMap];
        }

        $changedRanges = $rangesAfterCompleteMap;
    }

    return min(
        array_merge(
            ...array_map(
                static fn (Range $range): array => [$range->start, $range->end],
                $changedRanges
            )
        )
    );
}
