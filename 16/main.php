<?php

enum Direction: string
{
    case Up = 'up';
    case Right = 'right';
    case Down = 'down';
    case Left = 'left';
}

$input = array_map(
    'str_split',
    file($argv[1], FILE_IGNORE_NEW_LINES)
);

$energizedFromAllEntries = [];
$countRows = count($input);
$countColumns = count($input[0]);
for ($rowIndex = 0; $rowIndex < $countRows; $rowIndex++) {
    $energizedFromAllEntries[] = getCountEnergizedForBeam($input, $rowIndex, 0, Direction::Right);
    $energizedFromAllEntries[] = getCountEnergizedForBeam($input, $rowIndex, $countColumns - 1, Direction::Left);
}
for ($columnIndex = 0; $columnIndex < $countColumns; $columnIndex++) {
    $energizedFromAllEntries[] = getCountEnergizedForBeam($input, 0, $columnIndex, Direction::Down);
    $energizedFromAllEntries[] = getCountEnergizedForBeam($input, $countRows - 1, $columnIndex, Direction::Up);
}

$solution1 = getCountEnergizedForBeam($input, 0, 0, Direction::Right);
$solution2 = max($energizedFromAllEntries);

echo sprintf("Solution 16-1: %s\n", $solution1);
echo sprintf("Solution 16-2: %s\n", $solution2);

function getCountEnergizedForBeam(array $field, int $entryRow, int $entryColumn, Direction $beamDirection): int
{
    $energizedFields = [];
    $beamsToProcess = [[$entryRow, $entryColumn, $beamDirection]];
    while (!empty($beamsToProcess)) {
        $newBeams = [];
        foreach ($beamsToProcess as $beam) {
            $newBeams[] = applyBeam($field, $energizedFields, ...$beam);
        }

        $beamsToProcess = array_merge(...$newBeams);
    }

    return array_sum(
        array_map(
            'count',
            $energizedFields
        )
    );
}

function applyBeam(array $field, array &$energizedFields, int $entryRow, int $entryColumn, Direction $beamDirection): array
{
    $row = $entryRow;
    $column = $entryColumn;

    while (true) {
        $encounter = $field[$row][$column] ?? null;

        if ($encounter === null) {
            return [];
        }

        if (isset($energizedFields[$row][$column][$beamDirection->value])) {
            return [];
        }
        $energizedFields[$row][$column][$beamDirection->value] = 1;

        if ($encounter === '.') {
            match ($beamDirection) {
                Direction::Up => $row--,
                Direction::Down => $row++,
                Direction::Right => $column++,
                Direction::Left => $column--,
            };
            continue;
        }

        if ($encounter === '/') {
            return [match ($beamDirection) {
                Direction::Up => [$row, $column + 1, Direction::Right],
                Direction::Down => [$row, $column - 1, Direction::Left],
                Direction::Right => [$row - 1, $column, Direction::Up],
                Direction::Left => [$row + 1, $column, Direction::Down],
            }];

        }

        if ($encounter === '\\') {
            return [match ($beamDirection) {
                Direction::Up => [$row, $column - 1, Direction::Left],
                Direction::Down => [$row, $column + 1, Direction::Right],
                Direction::Right => [$row + 1, $column, Direction::Down],
                Direction::Left => [$row - 1, $column, Direction::Up],
            }];
        }

        if ($encounter === '|') {
            if ($beamDirection === Direction::Up || $beamDirection === Direction::Down) {
                match ($beamDirection) {
                    Direction::Up => $row--,
                    Direction::Down => $row++,
                };
                continue;
            }

            return [
                [$row - 1, $column, Direction::Up],
                [$row + 1, $column, Direction::Down],
            ];
        }

        if ($encounter === '-') {
            if ($beamDirection === Direction::Right || $beamDirection === Direction::Left) {
                match ($beamDirection) {
                    Direction::Right => $column++,
                    Direction::Left => $column--,
                };
                continue;
            }

            return [
                [$row, $column + 1, Direction::Right],
                [$row, $column - 1, Direction::Left],
            ];
        }
    }
}
