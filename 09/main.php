<?php

$input = file($argv[1], FILE_IGNORE_NEW_LINES);

$sequences = array_map(
    static fn(string $inputLine): array => array_map('intval', explode(' ', $inputLine)),
    $input
);
$reversedSequences = array_map('array_reverse', $sequences);

$solution1 = array_sum(array_map('calculateNextInSequence', $sequences));
$solution2 = array_sum(array_map('calculateNextInSequence', $reversedSequences));

echo sprintf("Solution 09-1: %s\n", $solution1);
echo sprintf("Solution 09-2: %s\n", $solution2);

function calculateNextInSequence(array $sequence): int
{
    if (count(array_unique($sequence)) === 1) {
        return $sequence[0];
    }

    $countSequence = count($sequence);

    $differences = [];
    for ($index = 0; $index < $countSequence - 1; $index++) {
        $differences[] = $sequence[$index + 1] - $sequence[$index];
    }

    return $sequence[$countSequence - 1] + calculateNextInSequence($differences);
}
