<?php

$inputString = trim(file_get_contents($argv[1]));
$fields = array_map(
    static fn(string $fieldString): array => explode("\n", $fieldString),
    explode("\n\n", $inputString)
);

$solution1 = array_sum(array_map('findReflectionInField', $fields));
$solution2 = array_sum(array_map('fixSmudgeAndFindReflection', $fields));

echo sprintf("Solution 13-1: %s\n", $solution1);
echo sprintf("Solution 13-2: %s\n", $solution2);

function fixSmudgeAndFindReflection(array $field): int
{
    $oldReflectionLine = findReflectionInField($field, false);
    foreach ($field as $rowIndex => $line) {
        foreach (str_split($line) as $columnIndex => $character) {
            $tempField = $field;
            $tempField[$rowIndex][$columnIndex] = $character === '#' ? '.' : '#';
            try {
                return findReflectionInField($tempField, true, $oldReflectionLine);
            } catch (LogicException) {
            }
        }
    }

    throw new LogicException();
}

function findReflectionInField(array $field, bool $returnOffset = true, ?string $ignoreReflectionLine = null): int|string
{
    $horizontalOffset = getOffsetForPerfectReflectionHorizontal($field);
    $reflectionLine = sprintf('h-%s', $horizontalOffset);
    if (null !== $horizontalOffset && (null === $ignoreReflectionLine || $ignoreReflectionLine !== $reflectionLine)) {
        return $returnOffset
            ? $horizontalOffset * 100
            : $reflectionLine;
    }
    $reversedHorizontalOffset = getOffsetForPerfectReflectionHorizontal(array_reverse($field));
    $reflectionLine = sprintf('r-%s', $reversedHorizontalOffset);
    if (null !== $reversedHorizontalOffset && (null === $ignoreReflectionLine || $ignoreReflectionLine !== $reflectionLine)) {
        return $returnOffset ?
            (count($field) - $reversedHorizontalOffset) * 100
            : $reflectionLine;
    }

    $transposedField = transposeField($field);
    $transposedHorizontalOffset = getOffsetForPerfectReflectionHorizontal($transposedField);
    $reflectionLine = sprintf('th-%s', $transposedHorizontalOffset);
    if (null !== $transposedHorizontalOffset && (null === $ignoreReflectionLine || $ignoreReflectionLine !== $reflectionLine)) {
        return $returnOffset
            ? $transposedHorizontalOffset
            : $reflectionLine;
    }

    $reversedTransposedHorizontalOffset = getOffsetForPerfectReflectionHorizontal(array_reverse($transposedField));
    $reflectionLine = sprintf('tr-%s', $reversedTransposedHorizontalOffset);
    if (null !== $reversedTransposedHorizontalOffset && (null === $ignoreReflectionLine || $ignoreReflectionLine !== $reflectionLine)) {
        return $returnOffset
            ? count($transposedField) - $reversedTransposedHorizontalOffset
            : $reflectionLine;
    }

    throw new LogicException();
}

function getOffsetForPerfectReflectionHorizontal(array $field): ?int
{
    $firstLine = $field[0];
    $firstLineDuplicates = array_filter(
        $field,
        static fn(string $line, int $key): bool => $key > 0 && $line === $firstLine,
        ARRAY_FILTER_USE_BOTH
    );

    foreach (array_keys($firstLineDuplicates) as $duplicateOffset) {
        if ($duplicateOffset % 2 === 0) {
            continue;
        }
        $offset = 0;
        while (true) {
            if ($duplicateOffset - $offset <= 0) {
                return $offset;
            }

            if ($field[$offset] !== $field[$duplicateOffset]) {
                break;
            }

            $offset++;
            $duplicateOffset--;
        }
    }

    return null;
}

function transposeField(array $field): array
{
    $arrayField = array_map('str_split', $field);
    $transposedArray = array_map(null, ... $arrayField);
    return array_map('implode', $transposedArray);
}
